package com.example.andre.temperature_master;

import android.content.res.Resources;
import android.support.annotation.StyleRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.SeekBar;
import android.view.Menu;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private SeekBar modeSeekBar;
    private TextView modeTextView;
    private SeekBar temperatureSeekBar;
    private TextView temperatureTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        modeSeekBar = (SeekBar) findViewById(R.id.modeSeekBar);
        modeTextView = (TextView) findViewById(R.id.currentMode);
        modeTextView.setFocusable(false);
        modeSeekBar.setOnSeekBarChangeListener(
                new OnSeekBarChangeListener() {
                    int progress = 0;
                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progressValue, boolean fromUser) {
                        progress = progressValue;
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // Do something here,
                        //if you want to do anything at the start of
                        // touching the seekbar
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // Display the value in textview
                        switch(modeSeekBar.getProgress()){
                            case 0:{modeTextView.setText("Ventilação");break;}
                            case 1:{modeTextView.setText("Aquecer");break;}
                            case 2:{modeTextView.setText("Resfriar");break;}
                            case 3:{modeTextView.setText("Automatico");break;}
                        }
                    }
                });
        temperatureSeekBar = (SeekBar) findViewById(R.id.temperatureSeekBar);
        temperatureTextView = (TextView) findViewById(R.id.currentTemperature);
        temperatureTextView.setFocusable(false);
        temperatureSeekBar.setOnSeekBarChangeListener(
                new OnSeekBarChangeListener() {
                    int progress = 0;
                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progressValue, boolean fromUser) {
                        progress = progressValue;
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // Do something here,
                        //if you want to do anything at the start of
                        // touching the seekbar
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // Display the value in textview
                        temperatureTextView.setText("" + (progress + 18));

                    }
                });
        TextView modeLabelTextView = (TextView) findViewById(R.id.editText3);
        modeLabelTextView.setFocusable(false);
        TextView tempLabelTextView = (TextView) findViewById(R.id.editText5);
        tempLabelTextView.setFocusable(false);
        TextView speedLabelTextView = (TextView) findViewById(R.id.editText4);
        speedLabelTextView.setFocusable(false);


    }


}
